import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Coursework1 {

    private static void process1HostName(String name){
        try{
            InetAddress inetAddress = InetAddress.getByName(name);
            System.out.println("HOST NAME: "+ inetAddress.getHostName());
            System.out.println("HOST ADDRESS: "+ inetAddress.getHostAddress());
            if (inetAddress instanceof Inet4Address){
                System.out.println("This is an IPv4 Address");
            }else if( inetAddress instanceof Inet6Address){
                System.out.println("This is an IPv6 Address");
            }
        }catch (UnknownHostException e){
            System.out.println("IP address error: ");
            e.printStackTrace();
        }
    }
    private static void process2HostNames(String name1, String name2){
        try{
            String prtText ="";
            InetAddress inetAddress1 = InetAddress.getByName(name1);
            InetAddress inetAddress2 = InetAddress.getByName(name2);
            if (inetAddress1 instanceof Inet4Address & inetAddress2 instanceof Inet4Address){ //i.e both are valid addresses
                byte[] bytesInet1 = inetAddress1.getAddress();
                byte[] bytesInet2 = inetAddress2.getAddress();
                int numberCnt = 0;
                for (int i=0; i<bytesInet1.length; i++){
                    if (bytesInet1[i] != bytesInet2[i])
                        break;
                    numberCnt++;

                }
                for (int i=0; i< numberCnt; i++){
                    if (bytesInet1[i]<0){
                        prtText += (256 + bytesInet1[i]);
                    }else
                        prtText += bytesInet1[i];
                    if (i!=bytesInet1.length -1){
                        prtText += ".";
                    }
                }
                for (int i= numberCnt; i<bytesInet1.length; i++){
                    if (i!= bytesInet1.length -1){
                        prtText += "*.";
                    }else{
                        prtText+="*";
                    }
                }
                System.out.println(prtText);
            }
            else{
                System.out.println("both of the addresses are not IPv4");
            }
        } catch (UnknownHostException e) {
            //throw error.
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        try{
            if (args.length ==1){
                process1HostName(args[0]);
            }else if(args.length ==2){
                process2HostNames(args[0],args[1]);
            }
        }catch (Exception e){
            System.out.println("Error: ");
            e.printStackTrace();
        }
    }
}
